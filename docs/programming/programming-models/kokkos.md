# Kokkos

Kokkos Core implements a programming model in C++ for writing
performance portable applications targeting all major HPC
platforms. For that purpose it provides abstractions for both parallel
execution of code and data management.  Kokkos is designed to target
complex node architectures with N-level memory hierarchies and
multiple types of execution resources. It currently can use OpenMP,
Pthreads and CUDA as backend programming models.

## Usage

For the latest information on using kokkos please see:

 * https://github.com/kokkos/kokkos
