##[HDF5](./hdf5)
A complete redesign of the Hierarchical Data Format, used to store
and organize large scientific datasets. Includes improved support for parallel
I/O.

##[HDF4](./hdf4)
The Hierarchical Data Format (HDF) is a set of file formats and libraries used
to store and organize large scientific datasets.

##[H5Py](./h5py)
A Pythonic interface to the HDF5 data format.

##[H5Hut](./h5hut)
H5hut (HDF5 Utility Toolkit) is a veneer API for HDF5 and is a superset of H5Part.
