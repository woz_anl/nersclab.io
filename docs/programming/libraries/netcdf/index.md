## [NetCDF](./netcdf)

NetCDF (network Common Data Form) is a set of libraries and machine-independent
data formats for creation, access, and sharing of array-oriented scientific
data. Includes the NCO, NCCMP, and CDO tools.
## [NCO](./nco)
The NetCDF Operators (NCO) are a suite of file operators that facilitate
manipulation and analysis of self-describing data stored in the NetCDF or
HDF4/5 formats.
## [CDO](./cdo)
Climate Data Operators (CDO) is a collection of command-line operators to
manipulate and analyze climate and forecast model Data.

## [NCView](./ncview)
NCView is a visual browser for NetCDF format files.

## [NetCDF4-python](./netcdfpython)
A python interface to the netCDF C library. 
