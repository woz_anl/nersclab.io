## Description and Overview
CDO is a large set of tools for working on climate data. [NetCDF](https://www.nersc.gov/users/data-analytics/data-management/i-o-libraries/netcdf-2/netcdf/)
3/4, GRIB (including SZIP compression), EXTRA, SERVICE, and IEG are supported
as I/O-formats. CDO can also be used to analyse any kind of gridded data not
related to climate science.
CDO has very small memory requirements and can process files larger than 
physical memory.

## How to Use CDO

```
module load cdo
cdo [options] Operators ...
```

## Documentation
[CDO Wiki](https://code.zmaw.de/projects/cdo/wiki/Cdo#Documentation)

## Availability

1.6.5.2, 1.7.0, 1.9.3(default)
