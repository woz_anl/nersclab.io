# Storage Statistics

Statistics about NERSC's storage systems are collected in order to
plan for future needs and to monitor utilization.  The statistics come
from HPSS collected statistics, HPSS accounting records and log files
provided by the hsi and ftp servers.

[Storage Statistics (nersc.gov)](http://www.nersc.gov/users/storage-and-file-systems/hpss/storage-statistics/)
