# Spin Containers Overview

## Overview

Spin is a new service platform at NERSC based on Docker container
technology.  It can be used to deploy web sites and science gateways,
workflow managers, databases and key-value stores, and all sorts of
network services that can access NERSC systems and storage on the back
end.

Spin is currently in a pilot phase as the NERSC Infrastructure
Services Group (ISG) refines the underlying systems and tools. The
initial release of these tools is scheduled for mid-May. Spin will
then be available for early access to users who complete the SpinUp
instructional program.

*Completion of the program is required for access!*

## Learn More

See the following video for a description of Spin, its capabilities,
and examples of services that are currently running there.

*This recording is from NERSC New User Training on March 21, 2018.*

<iframe width="560" height="315"
src="https://www.youtube.com/embed/bCpQgLsCQw0" frameborder="0"
allow="autoplay; encrypted-media" allowfullscreen></iframe>

See the following video for a live demonstration of a service being
built in Spin and basic maintenance tasks performed, such as changing
the underlying Docker images and accessing shell prompts and container
logs for troubleshooting.

<iframe width="560" height="315"
src="https://www.youtube.com/embed/U2jkCORlEfc" frameborder="0"
allow="autoplay; encrypted-media" allowfullscreen></iframe>

Want to prepare for using Spin? Read
the [Spin Getting Started Guide](getting_started).

## SpinUp Instructional Program

Users who want to try Spin during the pilot phase can apply to attend
the SpinUp program and learn how to build and deploy their own
services.

SpinUp includes three instructional sessions and three take-home
lessons. NERSC staff will lead the sessions and be on hand to help
throughout the program.  Participants will learn about the Spin
platform, create simple services on their laptops and deploy them in
Spin, and conclude by deploying their own custom service. Instruction
will also cover maintenance and troubleshooting techniques.

Because SpinUp is hands-on and interactive, space is
limited. Applicants must have an active NERSC allocation, a basic
understanding of Docker technology, and should have a service in mind
to deploy using Docker containers. While still in pilot, Spin may not
be suited yet for all types of services.  Applications are evaluated
based on their overall fit with current capabilities and on the
highest likelihood of a successful, working implementation.

Both local and remote participants are welcome.

To see the schedule of the next SpinUp program, please see
https://www.nersc.gov/users/data-analytics/spin/
