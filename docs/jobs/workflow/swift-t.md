#Swift/T Overview

The Swift/T scripting language is an alternate implementation of Swift
that runs in a single MPI job.  Swift/T has many performance
enhancements, such as the ability to call into Python or R scripts
directly without starting additional processes.

#Links

* [Swift/T Front Page](http://swift-lang.org/Swift-T)
* [Swift/T GitHub](https://github.com/swift-lang/swift-t)

#Access

##Login node

Type the following commands to run a simple Swift/T script on the login node:

```
$ PATH=$PATH:/global/common/software/nstaff/swift-t/login/2019-07-12/stc/bin
$ export PYTHONHOME=/usr/common/software/python/2.7-anaconda/envs/deeplearning
$ nice swift-t workflow.swift
```

##Compute node

Type the following commands to run a simple Swift/T script on the compute nodes:
```
$ PATH=$PATH:/global/common/software/nstaff/swift-t/compute/2019-07-12/stc/bin
# This will be pasted into the SLURM script
$ export TURBINE_DIRECTIVE="#SBATCH -C knl,quad,cache\n#SBATCH --license=SCRATCH"
$ swift-t -m slurm workflow.swift
```

#Example

The full example is at:
https://github.com/swift-lang/swift-work/tree/master/shell

This workflow shows how to create the typical workflow "diamond pattern".

Consider this `workflow.swift`:
```
// Define our external applications (apps):
app (file o) create(file i)
{
  "bash" "command.sh" i "--" o ;
}
app (file o) join(file i[])
{
  "bash" "command.sh" i "--" o ;
}

// Define our data:
file null = input("/dev/null");
file a<"a.txt">;
file b<"b.txt">;
file c<"c.txt">;
file d<"d.txt">;

// Define tasks to run and their dataflow:
// The following lines can be reordered arbitrarily:
a = create(null);
b = create(a);   // b and c can be created concurrently
c = create(a);  
d = join([b,c]); // this will fire after b,c are created
```

and this "user program" `command.sh`:
```
echo command.sh $*
sleep 1
touch $*
```

Given this workflow, file `a` will be created first,
then `b` and `c` concurrently, then `d` when both `b` and `c`
are complete.
